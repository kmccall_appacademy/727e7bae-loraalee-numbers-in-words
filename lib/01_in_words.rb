class Fixnum
  def in_words
    digits = to_array(self)
    if digits.length > 1
      last_nums = (digits[-2] * 10) + digits[-1]
    end

    if digits.length == 1
      zero_to_nine(self)
    elsif digits.length == 2
      tens_digits(self)
    elsif digits.length == 3
      delete_space("#{hundreds(digits[0])} #{tens_digits(last_nums)}")
    elsif digits.length <= 6
      thousands_num = digits[0...-3].join.to_i

      delete_space("#{thousands(thousands_num)} #{hundreds(digits[-3])} #{tens_digits(last_nums)}")
    elsif digits.length <= 9
      millions_num = digits[0...-6].join.to_i
      thousands_num = digits[-6...-3].join.to_i

      delete_space("#{millions(millions_num)} #{thousands(thousands_num)} #{hundreds(digits[-3])} #{tens_digits(last_nums)}")
    elsif digits.length <= 12
      billions_num = digits[0...-9].join.to_i
      puts billions_num
      millions_num = digits[-9...-6].join.to_i
      puts millions_num
      thousands_num = digits[-6...-3].join.to_i
      puts thousands_num

      delete_space("#{billions(billions_num)} #{millions(millions_num)} #{thousands(thousands_num)} #{hundreds(digits[-3])} #{tens_digits(last_nums)}")
    else
      trillions_num = digits[0...-12].join.to_i
      billions_num = digits[-12...-9].join.to_i
      millions_num = digits[-9...-6].join.to_i
      thousands_num = digits[-6...-3].join.to_i

      delete_space("#{trillions(trillions_num)} #{billions(billions_num)} #{millions(millions_num)} #{thousands(thousands_num)} #{hundreds(digits[-3])} #{tens_digits(last_nums)}")
    end

  end

  private

  def delete_space(string)
    string.split(" ").join(" ")
  end

  def hundreds(num)
    digits = to_array(num)

    if num == 0
      ""
    elsif num < 10
      "#{zero_to_nine(num)} hundred"
    elsif num > 100
      "#{zero_to_nine(digits.first)} hundred #{tens_digits(digits[1..2].join.to_i)}"
    end
  end

  def thousands(num)
    if num == 0
      ""
    elsif num < 10
      "#{zero_to_nine(num)} thousand"
    elsif num < 100
      "#{tens_digits(num)} thousand"
    else
      "#{hundreds(num)} thousand"
    end
  end

  def millions(num)
    if num == 0
      ""
    elsif num < 10
      "#{zero_to_nine(num)} million"
    elsif num < 100
      "#{tens_digits(num)} million"
    else
      "#{hundreds(num)} million"
    end
  end

  def billions(num)
    if num == 0
      ""
    elsif num < 10
      "#{zero_to_nine(num)} billion"
    elsif num < 100
      "#{tens_digits(num)} billion"
    else
      "#{hundreds(num)} billion"
    end
  end

  def trillions(num)
    if num == 0
      ""
    elsif num < 10
      "#{zero_to_nine(num)} trillion"
    elsif num < 100
      "#{tens_digits(num)} trillion"
    else
      "#{hundreds(num)} trillion"
    end
  end

  def tens_digits(num)
    if num == 0
      ""
    elsif num < 10
      zero_to_nine(num)
    elsif num < 13
      ten_to_twelve(num)
    elsif num < 20
      teens(num)
    else
      tens(num)
    end
  end

  def zero_to_nine(num)
    zero_to_nine = {
      0 => "zero",
      1 => "one",
      2 => "two",
      3 => "three",
      4 => "four",
      5 => "five",
      6 => "six",
      7 => "seven",
      8 => "eight",
      9 => "nine"
    }

    zero_to_nine[num]
  end

  def ten_to_twelve(num)
    ten_to_twelve = {
      10 => "ten",
      11 => "eleven",
      12 => "twelve"
    }

    ten_to_twelve[num]
  end

  def teens(num)
    prefixes = {
      3 => "thir",
      5 => "fif",
      8 => "eigh"
    }

    digits = to_array(num)

    if prefixes.key?(digits.last)
      prefixes[digits.last] + "teen"
    else
      zero_to_nine(digits.last) + "teen"
    end
  end

  def tens(num)
    digits = to_array(num)
    tens = {
      2 => "twenty",
      3 => "thirty",
      4 => "forty",
      5 => "fifty",
      6 => "sixty",
      7 => "seventy",
      8 => "eighty",
      9 => "ninety"
    }

    if digits.last == 0
      tens[digits.first]
    else
      tens[digits.first] + " " + zero_to_nine(digits.last)
    end
  end

  def to_array(num)
    num.to_s.split("").map { |str| str.to_i }
  end

end
